// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import
    Vue from 'vue'
import FastClick from 'fastclick'
import VueRouter from 'vue-router'
import App from './App'
import Home from './components/HelloFromVux'
import { WechatPlugin } from 'vux'
import vueresource from 'vue-resource'
import scan from './Scan'
import home from './Home'
import second from './second'
import Vuelidate from 'vuelidate'
import verify from "vue-verify-plugin";
Vue.use(VueRouter)
Vue.use(WechatPlugin)
Vue.use(vueresource)
Vue.use(Vuelidate)
Vue.use(verify,{
    blur:true
})

const routes = [
    {
  path: '/home',
  component: home

},
    {path:'/scan/:id', component:scan},
    {path:'/', component:home},
    {path:'/second/:id', component:second}
]

const router = new VueRouter({
  routes
})

FastClick.attach(document.body)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  router,
    methods:{


    },
  render: h => h(App)
}).$mount('#app-box')



